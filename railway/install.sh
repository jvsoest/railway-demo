# setup network
docker network create station

# download binary
curl -o station-0.0.1-SNAPSHOT.jar https://gitlab.com/medicaldataworks/railway/station/-/jobs/446031608/artifacts/raw/target/station-0.0.1-SNAPSHOT.jar\?job\=build

# add to railway station
username='johan_station'
usecret=''

access_token=$(curl -X POST -u "$username:$usecret" -d "grant_type=client_credentials" https://dev-keycloak.railway.medicaldataworks.nl/auth/realms/master/protocol/openid-connect/token | python3 -c "import sys, json; print(json.load(sys.stdin)['access_token'])")
response=$(curl --verbose --location --request POST https://dev.railway.medicaldataworks.nl/api/stations?access_token=$access_token --header 'Content-Type: application/json' --data-raw '{"name":"'$username'"}')
echo "add station response: $response"